﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace TreatmentsService
{
    [ServiceContract]
    public interface ITreatmentsService
    {

        [OperationContract]
        void AddTreatmentRecord(string name, decimal price);

        [OperationContract]
        List<TreatmentService> GetTreatmentRecords();

        [OperationContract]
        void UpdateTreatmentContact(int id, string name, decimal price);

        [OperationContract]
        void DeleteRecords(int id);

        [OperationContract]
        List<TratamientosInfo> Obtenertratamientos();

        [OperationContract]
        List<TratamientosInfo> GetPriceNameTreatmens(int id);   
    }


    [DataContract]
    public class TratamientosInfo
    {
        [DataMember]
        public int IDTratamiento { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }

}
