﻿using System.Data.Linq.Mapping;

namespace TreatmentsService
{

        [Table(Name = "Treatment")]
        public class TreatmentService
        {
            [Column(IsPrimaryKey = true, IsDbGenerated = true, Name = "ID")]
            public int ID { get; set; }

            [Column(Name = "Name")]
            public string Name { get; set; }

            [Column(Name = "Price")]
            public decimal Price { get; set; }
        }
}