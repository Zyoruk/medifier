﻿using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace TreatmentsService
{
    [Database(Name = "Treatments")]
    public class Tratamiento : DataContext
    {
        public Tratamiento() : base(Configuration.ConnectionString) { }
        public Table<TreatmentService> Treatment;
    }
}