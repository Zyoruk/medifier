﻿using System.Collections.Generic;
using System.Linq;

namespace TreatmentsService
{
    public class TreatmentsService : ITreatmentsService
    {
        public static decimal price;
        Tratamiento bd = new Tratamiento();
        public void AddTreatmentRecord(string name, decimal price)
        {
            TreatmentService treatment
            = new TreatmentService { Name = name, Price = price};
            bd.Treatment.InsertOnSubmit(treatment);
            bd.SubmitChanges();
        }

        public List<TreatmentService> GetTreatmentRecords()
        {
            var consulta = from cl in bd.Treatment
                           select cl;
            return consulta.ToList();
        }

        public List<TratamientosInfo> GetPriceNameTreatmens(int id)
        {
            
            var consulta = from cl in bd.Treatment
                           where cl.ID.Equals(id)
                           select new { cl.Price, cl.Name };

            List<TratamientosInfo> tratamientos = new List<TratamientosInfo>();

            foreach (var Dato in consulta)
            {
                tratamientos.Add(new TratamientosInfo
                {
                    Price = Dato.Price,
                    Name = Dato.Name
                });
            }
           
            return tratamientos;
        }

        //Delete Record  
        public void DeleteRecords(int id)
        {
            TreatmentService treatmentService = bd.Treatment.Single(c => c.ID == id);
            bd.Treatment.DeleteOnSubmit(treatmentService);
            bd.SubmitChanges();
        }

        public void UpdateTreatmentContact(int id, string name, decimal price)
        {
            TreatmentService treatmentService = bd.Treatment.Single(c => c.ID == id);
            treatmentService.Name = name;
            treatmentService.Price = price;
            bd.SubmitChanges();
        }

        public List<TratamientosInfo> Obtenertratamientos()
        {
            var consulta = from cl in bd.Treatment
                           select cl;

            List<TratamientosInfo> Dtratamientos = new List<TratamientosInfo>();

            foreach (var tratamientos in consulta)
            {
                Dtratamientos.Add(new TratamientosInfo
                {
                    IDTratamiento = tratamientos.ID,
                    Name = tratamientos.Name
                });
            }
            return Dtratamientos;
        }
    }
}
