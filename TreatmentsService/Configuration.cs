﻿using System.Configuration;

namespace TreatmentsService
{
    public class Configuration
    {
        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager
                    .ConnectionStrings["conStr"]
                    .ConnectionString.ToString();
            }
        }
    }
}