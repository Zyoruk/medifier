﻿using System.Configuration;


namespace AccountingService
{
    public class Configuration
    {
        public static string CadenaConexion
        {
            get
            {
                return
                 ConfigurationManager.
                 ConnectionStrings["Cadenaconexion"].
                 ConnectionString.ToString();
            }
        }
    }
}