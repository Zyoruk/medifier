﻿using AccountingService.Model;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AccountingService
{
  public class AccountingService : IAccountingService
    {
        AccountingData bd = new AccountingData();

        public void AgregarFactura(string cedula, string nombretratamiento, decimal precio)
        {
            CobroModel nuevaFactura = new CobroModel
            {
                Nametrata = nombretratamiento,
                Costo = precio,
                Iddocument = cedula,
                Date = DateTime.Parse(DateTime.Today.ToString("dd/MM/yyyy")),
                Estado = "pendiente",
            };
            bd.CobroModels.InsertOnSubmit(nuevaFactura);
            bd.SubmitChanges();
        }


        public List<CobroModel> GetFactData(string id)
        {
            var consulta = from cl in bd.CobroModels
                           where cl.Iddocument == id && cl.Estado =="pendiente"
                           select cl;

             return consulta.ToList();
        }

        public void SaveDataFact(int idrecibo, string iduser, string formapago)
        {

            CobroModel data = bd.CobroModels.Single(c => c.Iddocument == iduser && c.Idrecibo == idrecibo);
            data.Estado = "Cancelado";
            data.FormaPago = formapago;
            bd.SubmitChanges();

        }
    }
}
