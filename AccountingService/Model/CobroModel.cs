﻿using System;
using System.Data.Linq.Mapping;

namespace AccountingService.Model
{
    [Table(Name = "Cobros")]
    public class CobroModel
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Idrecibo { get; set; }

        [Column]
        public string Nametrata { get; set; }

        [Column]
        public decimal Costo { get; set; }

        [Column]
        public string Iddocument { get; set; }

        [Column]
        public DateTime Date { get; set; } 

        [Column]
        public string Estado { get; set; }

        [Column]
        public string FormaPago { get; set; }



    }
}