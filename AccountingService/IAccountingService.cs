﻿using AccountingService.Model;
using System.Collections.Generic;
using System.ServiceModel;

namespace AccountingService
{
    [ServiceContract]
    public interface IAccountingService
    {

        [OperationContract]
        void AgregarFactura(string cedula, string nombretratamiento, decimal precio);

        [OperationContract]
        List<CobroModel> GetFactData(string id);

        [OperationContract]
        void SaveDataFact(int idrecibo, string iduser, string formapago);
    }


}
