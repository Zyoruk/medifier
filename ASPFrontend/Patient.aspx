﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_OutterPage.Master" CodeBehind="Patient.aspx.cs" Inherits="ASPFrontend.Patient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Pacientes</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <h1>Pacientes</h1>
    <asp:GridView ID="GridView" class="table table-responsive table-striped table-hover w-100" runat="server" AutoGenerateColumns="False" DataKeyNames="DocumentId" CellPadding="5"  OnSelectedIndexChanged="GridView_SelectedIndexChanged">
        <HeaderStyle CssClass="thead-dark" />
        <Columns>
            <asp:BoundField DataField="DocumentId" HeaderText="Número de identificación" />
            <asp:BoundField DataField="FirstName" HeaderText="Nombre" />
            <asp:BoundField DataField="LastName" HeaderText="Apellidos" />
            <asp:BoundField DataField="Phone" HeaderText="Telefono" />
        </Columns>
    </asp:GridView>
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" class=" w-100 table table-responsive table-striped table-hover">
        <tr>
            <td align:center>Nombre:<br />
                <asp:TextBox ID="txtName" runat="server" Width="210" />
                <br />
            </td>
            <td align:center>Apellido:<br />
                <asp:TextBox ID="txtLastName" runat="server" Width="210" />
                <br />
            </td>
            <td align:center>Cedula de identidad:<br />
                <asp:TextBox ID="txtDocument" runat="server" Width="210" TextMode="Number" MaxLength="9" />
                <br />
            </td>
        </tr>
        <tr>
            <td align:center>Número de teléfono:<br />
                <asp:TextBox ID="txtPhone" runat="server" Width="210" TextMode="Number" MaxLength="8" />
                <br />
            </td>
            <td align:center>Correo:<br />
                <asp:TextBox ID="txtEmail" runat="server" Width="210" TextMode="Email" />
                <br />
            </td>
            <td align:center>Fecha de cumpleaños:<br />
                <asp:TextBox ID="txtBirth" runat="server" Width="210" TextMode="Date" />
                <br />
            </td>
            <td align="center">
                <asp:Button ID="btnAdd" runat="server" Text="Agregar" OnClick="Insert" BackColor="#006666" CssClass="btn btn-primary" />
            </td>
        </tr>
    </table>
    <asp:Label ID="lblError" runat="server" ForeColor="Red" EnableViewState="False" />
    <asp:Label ID="lblSuccess" runat="server" ForeColor="Green" EnableViewState="False" />
</asp:Content>
