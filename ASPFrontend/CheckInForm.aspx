﻿<%@ Page Title="" Language="C#" MasterPageFile="_OutterPage.master" AutoEventWireup="true" CodeBehind="CheckInForm.aspx.cs" Inherits="ASPFrontend.CheckInForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">

    <div class="container-md">

            <div class="row">
                <div class="col-sm-3">
                    <h2 style="color: #e4a81c; text-align: center;">Medifier</h2>
                    <h5 style="color: #e4a81c; text-align: center;">Facturacion</h5>
                </div>
            </div>

            <div class="row" id="headeruno">
                <div class="col-xs-6 col-md-4 col-sm-2">
                    <label>ID Paciente</label>
                    <div class="input-group" id="buscador">
                        <asp:TextBox ID="documentpacienttxt" runat="server" class="form-control" placeholder="Ingrese el ID del paciente"></asp:TextBox>
                        <span class="input-group-btn">
                            <asp:Button ID="Button2" runat="server" Text="Buscar" class="btn btn-info" OnClick="Button2_Click" />
                        </span>
                    </div>
                </div>

        <div class="col-4"></div>

        <div class="col-xs-6 col-md-4 col-sm-2">
            <label>Forma de pago</label>
            <asp:DropDownList ID="DropDownformapago" runat="server" Enabled="true" AutoPostBack="false" class="form-control">
                <asp:ListItem Value="N/A" Selected="true">--- Seleccione ---</asp:ListItem>
                <asp:ListItem Value="SINPEMOVIL">SINPE MOVIL</asp:ListItem>
                <asp:ListItem Value="EFECTIVO">EFECTIVO</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="row" id="headerdos">

        <div class="col-xs-6 col-md-4 col-sm-2">
            <label>ID Paciente</label>
            <asp:TextBox ID="idpacientetxt" runat="server" class="form-control" placeholder="Id del paicente" ReadOnly="True"></asp:TextBox>
        </div>

        <div class="col-xs-6 col-md-4 col-sm-2">
            <label>Paciente</label>
            <asp:TextBox ID="pacientetxt" runat="server" class="form-control" placeholder="Nombre del paciente" ReadOnly="True"></asp:TextBox>
        </div>

        <div class="col-xs-6 col-md-4 col-sm-2">
            <label>Correo</label>
            <asp:TextBox ID="correotxt" runat="server" class="form-control" placeholder="Correo electronico" ReadOnly="True" TextMode="Email"></asp:TextBox>
        </div>

    </div>


        <div class="row" id="gv">
            <asp:GridView ShowHeaderWhenEmpty="True" ID="gvcitasdetails" runat="server" AutoGenerateColumns="False" class="table table-success table-striped" AllowPaging="True" PageSize="5">
                <Columns>
                    <asp:BoundField HeaderText="RECIBO" DataField="Idrecibo" />
                    <asp:BoundField HeaderText="TRATAMIENTO" DataField="Nametrata" />
                    <asp:BoundField HeaderText="COSTO" DataField="Costo" DataFormatString="¢{0:#,#}" />
                    <asp:BoundField HeaderText="FECHA" DataField="Date" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="gvCheckBox" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

                <PagerSettings FirstPageText="&amp;gt;&amp;gt;" PageButtonCount="5" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />

            </asp:GridView>
        </div>


        <asp:Button runat="server" ID="btnsaveCheckInForm" OnClick="btnsaveCheckInForm_Click" text="Facturar"/>

        <hr />
        <asp:Label ID="Mensaje" runat="server" Text="Label"></asp:Label>
    </div>

</asp:Content>
