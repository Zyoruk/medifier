﻿using System;
using System.Configuration;

namespace ASPFrontend.Wrappers
{
    public class ConfigurationWrapper
    {
        public static string ReadAppSetting(string key)
        {
            try
            {
                string result = ConfigurationManager.AppSettings[key];
                if (string.IsNullOrEmpty(result))
                {
                    throw new Exception("Cannot read setting");
                }
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                throw new Exception("Error reading app settings");
            }
        }
    }
}