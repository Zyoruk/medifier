﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ASPFrontend.Views.Login"   MasterPageFile="_OutterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <title>Login</title>
    <style type="text/css">
        .loginContainer { 
            height: calc(100vh - 56px);
            padding: 20px 0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">
    <div class="d-flex justify-content-center loginContainer">
        <div class="d-flex flex-column w-50">
            <div class="mb-3">
                <asp:Label ID="lblUsername" runat="server" Text="Username" CssClass="form-label" AssociatedControlID="txtUsername"></asp:Label>
                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" ></asp:TextBox>
            </div>
            <div class="mb-3">
                <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="form-label" AssociatedControlID="txtPassword"></asp:Label>
                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
            </div>
            <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="btn btn-primary" OnClick="btnLogin_Click"/>
            <br />
            <asp:Label ID="lblError" runat="server" CssClass="form-label" ForeColor="Red"></asp:Label>
        </div>
    </div>
</asp:Content>