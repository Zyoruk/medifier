﻿using ASPFrontend.ApiGateway;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace ASPFrontend
{
    public partial class CheckInForm1 : System.Web.UI.Page
    {
        ApiGatewayClient api = new ApiGatewayClient();

        public static string iddocument;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (IsPostBack)
            //{
            //    cargarDatos();
            //}
                DropDownformapago.Items[0].Attributes["disabled"] = "disabled";
                Mensaje.Visible = false;
            

        }


        protected void Button2_Click(object sender, EventArgs e)
        {

            cargarDatos();
        }


        public void savedata() {
            foreach (GridViewRow item in gvcitasdetails.Rows)
            {
                CheckBox chk = (CheckBox)item.FindControl("gvCheckBox");
                if (chk != null && chk.Checked && DropDownformapago.SelectedValue != "N/A")
                {
                   
                        int idrecibo = Int32.Parse(item.Cells[0].Text);
                        string iduser = iddocument;
                        string formapago = DropDownformapago.SelectedValue;
                        api.SaveDataFact(idrecibo, iduser, formapago);

                        Mensaje.Text = "Facturado corectamente. ";
                        Mensaje.Visible = true;

                    cargarDatos();
                    
                }
                else
                {
                    Mensaje.Text = "Seleccione factura por cobrar y metodo de pago ";
                    Mensaje.Visible = true;
                }
            }

        }

        protected void btnsaveCheckInForm_Click(object sender, EventArgs e)
        {
            savedata();
        }

        public void cargarDatos()
        {
            iddocument = documentpacienttxt.Text.Trim();

            UserDataModel dt = api.UserDataModel(iddocument);

            idpacientetxt.Text = dt.User.ToString();
            correotxt.Text = dt.Email;
            pacientetxt.Text = dt.Firtsname + " " + dt.Lastname;

            DataTable finalDT = new DataTable();
            finalDT.Columns.Add(new DataColumn("Idrecibo"));
            finalDT.Columns.Add(new DataColumn("Nametrata"));
            finalDT.Columns.Add(new DataColumn("Costo"));
            finalDT.Columns.Add(new DataColumn("Date"));

            foreach (var item in dt.Cobros)
            {
                var dr = finalDT.NewRow();
                dr["Idrecibo"] = item.Idrecibo;
                dr["Nametrata"] = item.Nametrata;
                dr["Costo"] = item.Costo;
                dr["Date"] = item.Date;
                finalDT.Rows.Add(dr);
            }

            gvcitasdetails.DataSource = finalDT;
            gvcitasdetails.DataBind();
        }
    }
}