﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPFrontend
{
    public partial class Tratamiento : System.Web.UI.Page
    {

        ApiGateway.ApiGatewayClient obj = new ApiGateway.ApiGatewayClient();
        private void ClearControls()
        {
            txtName3.Text = string.Empty;
            txtPrice3.Text = string.Empty;
            btnAdd.Text = "Agregar";
        }
        private void BindTreatmentDetails(int Id)
        {
            
        }
        private void SaveTreatmentDetails()
        {
            if(string.IsNullOrEmpty(txtName3.Text) || string.IsNullOrEmpty(txtPrice3.Text))
            {
                lblstatus.Text = "Ingrese la información faltante, por favor!";
            }
            else
            {
            string name = txtName3.Text.Trim();
            decimal price = Convert.ToDecimal(txtPrice3.Text.Trim());
            obj.AddTreatmentRecord(name,price);
            }
        }
        private void UpdateTreatmentDetails()
        {

        } 

        private void ShowData()
        {
            BindTreatmentDetails(0);
            ClearControls();
            
            grdWcfTest.DataSource = obj.GetTreatmentRecords();
            grdWcfTest.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                ShowData();
            }

            
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }
        protected void lnkEdit_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            
        }
        protected void lnkDelete_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        protected void grdWcfTest_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(grdWcfTest.Rows[e.RowIndex].Cells[0].Text);
                obj.DeleteTreatment(id);
            }
            catch(Exception ex)
            {
                Response.Write(ex.Message.ToString());
            }
            finally
            {
                grdWcfTest.DataSource = obj.GetTreatmentRecords();
                grdWcfTest.DataBind();
            }
        }

        protected void grdWcfTest_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grdWcfTest_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdWcfTest.EditIndex = e.NewEditIndex;
            grdWcfTest.DataSource = obj.GetTreatmentRecords();
            grdWcfTest.DataBind();
        }

        protected void grdWcfTest_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = Convert.ToInt32(((TextBox)(grdWcfTest.Rows[e.RowIndex].Cells[0].Controls[0])).Text);
            string name = ((TextBox)(grdWcfTest.Rows[e.RowIndex].Cells[1].Controls[0])).Text;
            
            var row = grdWcfTest.Rows[e.RowIndex].Cells[2].Controls[0];
            var textBox = (TextBox)row;
            var textBoxText = textBox.Text;
            double price = double.Parse(string.IsNullOrEmpty(textBoxText) ? "0" : textBoxText);

            try
            {
                if (String.IsNullOrEmpty(name) || price == 0.0)
                {
                    lblstatus.Text = "No se modificó registro";
                }
                else
                {
                    obj.UpdateTreatmentRecord(id, name, Decimal.Parse(price.ToString()));
                }   
            }
            finally
            {
                grdWcfTest.EditIndex = -1;
                grdWcfTest.DataSource = obj.GetTreatmentRecords();
                grdWcfTest.DataBind();
            }
        }

        protected void Insert(object sender, EventArgs e)
        {
            //txtPrice3.Attributes.Add("onKeypress", "javascript:SoloNumeros()");
            //txtName3.Attributes.Add("onKeypress", "javascript:SoloLetras()");
            SaveTreatmentDetails();
            ShowData();
        }

        protected void grdWcfTest_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdWcfTest.EditIndex = -1;
            ShowData();
        }
    }
}