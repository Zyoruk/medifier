﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_OutterPage.Master" AutoEventWireup="true" CodeBehind="AccountingMainForm.aspx.cs" Inherits="ASPFrontend.CheckInForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <title>Accounting Main Form</title>
      <link href="css/AccountingStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">

    <div class="row justify-content-center" id="optionaccounting">

        <div class="col-4">
            <a href="CheckInForm.aspx" id="btntranscday" type="button" class="btn btn-secondary">TRANSACCIONES DIARIAS</a>
        </div>
        <div class="col-4">
            <a href="#" id="btndaygroup" type="button" class="btn btn-secondary">CUENTAS POR COBRAR</a>
        </div>
    </div>

</asp:Content>
