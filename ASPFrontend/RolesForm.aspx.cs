﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace ASPFrontend
{
    public partial class RolesForm : System.Web.UI.Page
    {
        ApiGateway.ApiGatewayClient api = new ApiGateway.ApiGatewayClient();

        protected void Page_Load(object sender, EventArgs e)
        {
            Mensaje.Visible = false;
        }

        protected void AddOne_Click(object sender, EventArgs e)
        {
            while (ListBoxRoles.GetSelectedIndices().Length > 0)
            {

                ListBoxRolesUser.Items.Add(ListBoxRoles.SelectedItem);
                ListBoxRoles.Items.Remove(ListBoxRoles.SelectedItem);
            }
        }

        protected void PutOffOne_Click(object sender, EventArgs e)
        {
            while (ListBoxRolesUser.GetSelectedIndices().Length > 0)
            {
                ListBoxRoles.Items.Add(ListBoxRolesUser.SelectedItem);
                ListBoxRolesUser.Items.Remove(ListBoxRolesUser.SelectedItem);
            }
        }

        protected void btnbuscar_Click(object sender, EventArgs e)
        {
            if (DocumentIDtxt.Text != String.Empty) // se valida que no sea null
            {
                cargardatos(DocumentIDtxt.Text);
            }
            else
            {
                Mensaje.Text = "You must enter the ID Document User";
                Mensaje.Visible = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                ActualizarRolesUsuario();
                Mensaje.Text = "Roles updated successfully";
                Mensaje.Visible = true;
            }
            catch (Exception)
            {
                Mensaje.Text = "Error";
                Mensaje.Visible = true;
            }

        }

        public void cargardatos(string iddoc)
        {
            ApiGateway.ApiModel dt = api.GetDataApi(iddoc);

            if (dt.Id != 0 && dt.FirstName != null && dt.DocumentId != null)
            {
                UserId.Text = Convert.ToString(dt.Id);
                UserName.Text = dt.FirstName + " " + dt.LastName;

                // se cargan los list con los datos obtenidos de las referencias obtenidas
                string[] systemRoles = api.GetApiRol()
                    .Select(rol => rol.Rol)
                    .ToArray();

                string[] userRoles = dt.Roles.Split(',');

                ListBoxRolesUser.Items.Clear();
                ListBoxRoles.Items.Clear();

                foreach (string systemRol in systemRoles)
                {
                    if (userRoles.Contains(systemRol))
                    {
                        ListBoxRolesUser.Items.Add(systemRol);
                    }
                    else
                    {
                        ListBoxRoles.Items.Add(systemRol);
                    }
                }
            }
            else
            {
                Mensaje.Text = "Unregistered user";
                Mensaje.Visible = true;
            }
        }

        public void ActualizarRolesUsuario()
        {
            string[] selectedRoles = ListBoxRolesUser.Items.Cast<ListItem>().Select(l => l.Value).ToArray();
            api.UpdateRoles(Convert.ToInt32(UserId.Text), selectedRoles);
        }
    }
}