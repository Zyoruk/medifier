﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_OutterPage.Master" AutoEventWireup="true" CodeBehind="RolesForm.aspx.cs" Inherits="ASPFrontend.RolesForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <h1>Roles de usuario</h1>
    <div class="col-sm-4">
        <span>Identificación:</span>
        <div class="input-group">
            <asp:TextBox ID="DocumentIDtxt" runat="server" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Ingrese el documento de indentificación"></asp:TextBox>
            <span class="input-group-btn">
                <asp:LinkButton ID="btnbuscar" runat="server" OnClick="btnbuscar_Click" class="btn btn-primary btn-lg">
                                <i class="bi bi-search"></i>
                </asp:LinkButton>
            </span>
        </div>
        <br />
        <br />
    </div>



    <div class="row">

        <span>Id de usuario:    
            <asp:Label ID="UserId" runat="server"></asp:Label></span>
        <br />
        <span>Nombre:  
            <asp:Label ID="UserName" runat="server"></asp:Label></span>
        <br />
        <br />
    </div>


    <div class="row">


        <div class="col-sm-3">
            <span>Roles disponibles</span>
            <br />
            <asp:ListBox ID="ListBoxRoles" runat="server" SelectionMode="Multiple" Height="127px" Width="234px"></asp:ListBox>

        </div>

        <div class="col-sm-1 flex-column d-flex justify-content-center">
            <asp:LinkButton ID="AddOne" runat="server" Text="+" OnClick="AddOne_Click" ToolTip="Add role" CssClass="btn btn-success mb-3 col-sm-6">
                    <i class="bi bi-plus"></i>
            </asp:LinkButton>
            <asp:LinkButton ID="PutOffOne" runat="server" Text="-" OnClick="PutOffOne_Click" ToolTip="Remove role" CssClass="btn btn-danger col-sm-6">
                    <i class="bi bi-dash"></i>
            </asp:LinkButton>

        </div>

        <div class="col-sm-3">
            <span>Roles seleccionados</span>
            <br />
            <asp:ListBox ID="ListBoxRolesUser" runat="server" SelectionMode="Multiple" Height="127px" Width="234px"></asp:ListBox>
        </div>

        <br />
        <br />

        <div class="col-sm-9">
            <br />
            <div class="input-group">
                <span class="input-group-btn">
                    <asp:Button ID="Button1" runat="server" Text="Guardar" OnClick="Button1_Click" class="btn btn-primary" />
                </span>
            </div>

        </div>
        <asp:Label ID="Mensaje" runat="server" Text="Label"></asp:Label>
        <br />

    </div>
</asp:Content>
