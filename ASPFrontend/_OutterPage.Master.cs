﻿using ASPFrontend.Services;
using System;
using System.Web.UI;

namespace ASPFrontend.Views.Shared
{
    public partial class _OutterPage : MasterPage
    {
        public  _BasePage page = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.page = new _BasePage(Request.Url.AbsolutePath, Session);

            if (SessionService.GetSessionInfo(Session) == null && !page.IsPageOutter)
            {
                Response.Redirect("Login.aspx");
            }

            if (SessionService.GetSessionInfo(Session) != null && page.IsPageOutter)
            {
                Response.Redirect(Config.DefaultPage);
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");

        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Session["token"] = null;
            Response.Redirect("/");
        }
    }
}