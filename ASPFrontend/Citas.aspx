﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Citas.aspx.cs" Inherits="ASPFrontend.Citas" MasterPageFile="_OutterPage.master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <title>Citas</title>

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">
    <asp:Label ID="Label1" runat="server" Text="Cedula: "></asp:Label>
    <asp:TextBox ID="txtCedula" runat="server"></asp:TextBox>
    <asp:Button ID="BtnBuscar" runat="server" Text="Buscar" OnClick="BtnBuscar_Click" />
    <h1>Citas</h1>
    <div class="table-responsive">
        <asp:GridView ID="gvcitas" runat="server" AutoGenerateColumns="false"
            ShowFooter="true"
            DataKeyNames="IntIdCita"
            ShowHeaderWhenEmpty="true"

            OnRowCommand="gvcitas_RowCommand" OnRowEditing="gvcitas_RowEditing" OnRowCancelingEdit="gvcitas_RowCancelingEdit"
            OnRowUpdating="gvcitas_RowUpdating" OnRowDeleting="gvcitas_RowDeleting"

            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Height="220px" Width="683px" OnRowDataBound="gvcitas_RowDataBound1">
            <%-- Theme Properties --%>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
                
            <Columns>
                <asp:TemplateField HeaderText="Id Cita ">

                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("IntIdCita") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cedula del Paciente">

                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("StrCedula") %>' runat="server" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox CssClass="form-control" ID="txtStrCedula" Text='<%# Eval("StrCedula") %>' runat="server" />
                    </EditItemTemplate>
                    <FooterTemplate>
                        <% if (IsAuth)
                            { %>
                        <asp:TextBox CssClass="form-control" ID="txtStrCedulaFooter" runat="server"
                            Text='<%# page.SessionInfo.Roles.Contains("Paciente") && !IsAuth ? page.SessionInfo.DocumentId : "" %>'
                            Enabled='<%# page.SessionInfo.Roles.Contains("Paciente") &&!IsAuth ? false  : true %>' />
                        <%} %>
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Nombre Paciente">

                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("NombrePaciente") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Fecha Cita">

                    <ItemTemplate>
                        <asp:Label Text='<%# Eval("DateFechaCita") %>' runat="server" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDateFechaCita" CssClass="form-control" Text='<%# Eval("DateFechaCita") %>' runat="server"/>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <% if (IsAuth)
                            { %>
                        <asp:TextBox CssClass="form-control" ID="txtDateFechaCitaFooter" TextMode="Date" runat="server" />
                        <%} %>
                    </FooterTemplate>
                </asp:TemplateField>

                    <asp:TemplateField HeaderText="Hora Cita">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("CitaHora") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            
                            <asp:TextBox ID="txtCitaHora" Text='<%# Eval("CitaHora") %>' runat="server" Enabled="False" />
                            <asp:DropDownList ID="DropHoraEdit" runat="server"></asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="DropHoras" runat="server"></asp:DropDownList>
                           <%-- <asp:TextBox ID="txtCitaHoraFooter"  runat="server"  />--%>
                        </FooterTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Tratamiento">
                        <ItemTemplate>
                           <%-- <asp:DropDownList DataTextFormatString='<%# Eval("IntIdTratamiento") %>' ID="DropDownList1" runat="server"></asp:DropDownList>--%>
                            <asp:Label Text='<%# Eval("NameTratamiento") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
   
                            <asp:TextBox ID="txtIntIdTratamiento" Text='<%# Eval("NameTratamiento") %>' runat="server" Enabled="False" /> 
                            <asp:DropDownList ID="DropEditTratamiento" runat="server"></asp:DropDownList>
                        </EditItemTemplate>

                        <FooterTemplate>
                             <asp:DropDownList ID="DropDownList1" runat="server" ></asp:DropDownList>
                           <%-- <asp:TextBox ID="txtIntIdTratamientoFooter" runat="server" />--%>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Precio total">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("TrataPreciototal ") %>' runat="server" />
                        </ItemTemplate>
                       <%-- <EditItemTemplate>
                            <asp:TextBox ID="txtTrataPreciototal" Text='<%# Eval("TrataPreciototal") %>' runat="server" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            
                            <asp:TextBox ID="txtTrataPreciototalFooter" runat="server" TextMode="Number" />
                        </FooterTemplate>--%>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Estado">
                        <ItemTemplate>
                            <asp:Label Text='<%# Eval("Estado") %>' runat="server" />
                        </ItemTemplate>
                        <EditItemTemplate>
                         
                              <asp:DropDownList ID="DropEstado" runat="server">
                                  <asp:ListItem Text="Pendiente" Value="Pendiente" /> 
                                  <asp:ListItem Text="Atendido" Value="Atendido" />
                              </asp:DropDownList>
                        
                        </EditItemTemplate>
                      <%--  <FooterTemplate>
                            
                            <asp:TextBox ID="txtEstadoFooter" runat="server"  />
                        </FooterTemplate>--%>
                    </asp:TemplateField>

                <asp:TemplateField>

                    <ItemTemplate>
                        <% if (IsAuth)
                            { %>
                        <asp:LinkButton runat="server" CommandName="Edit" CssClass="btn btn-warning btn-sm mb-2" ToolTip="Editar">
                            <i class="bi bi-pen"></i>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CommandName="Delete" CssClass="btn btn-danger btn-sm" ToolTip="Eliminar">
                            <i class="bi bi-trash"></i>
                        </asp:LinkButton>
                        
                        <% } %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <% if (IsAuth)
                            { %>
                        <asp:LinkButton runat="server" CommandName="Update" CssClass="btn btn-primary btn-sm mb-2" ToolTip="Guardar">
                            <i class="bi bi-save"></i>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CommandName="Cancel" CssClass="btn btn-danger btn-sm" ToolTip="Cancelar">
                            <i class="bi bi-x"></i>
                        </asp:LinkButton>
                        <% } %>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <% if (IsAuth)
                            { %>
                            <asp:LinkButton runat="server" CommandName="AddNew" CssClass="btn btn-success btn-sm" ToolTip="Agregar">
                                <i class="bi bi-plus"></i>
                            </asp:LinkButton>
                        <%} %>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
