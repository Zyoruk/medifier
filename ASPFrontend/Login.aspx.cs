﻿using ASPFrontend.Services;
using System;

namespace ASPFrontend.Views
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            string md5 = AppUtils.GetMD5(password);
            try
            {
                string token = ApiGatewayService.Instance.Login(username, md5);
                if (token != null)
                {
                    SessionService.SetSessionToken(token, Session);
                    Response.Redirect(Config.DefaultPage);
                } else
                {
                    lblError.Text = "Credenciales incorrectas.";
                }
            }
            catch
            {
                lblError.Text = "Credenciales incorrectas.";
            }  
        }
    }
}