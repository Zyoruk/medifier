﻿
using ASPFrontend.ApiGateway;
using ASPFrontend.Services;
using System.Collections.Generic;
using System.Web.SessionState;

namespace ASPFrontend
{
    public partial class _BasePage
    {
        public static List<string> OUTTER_PAGES = new List<string> {
            "/", "/Login.aspx"
        };


        private string AbsolutePath { get; }
        private HttpSessionState Session { get; }

        public _BasePage(string AbsolutePath, HttpSessionState Session)
        {
            this.AbsolutePath = AbsolutePath ?? throw new System.ArgumentNullException(nameof(AbsolutePath));
            this.Session = Session ?? throw new System.ArgumentNullException(nameof(Session));
        }

        public SessionInfo SessionInfo
        {
            get
            {
                var info = SessionService.GetSessionInfo(Session);
                if (info != null)
                {
                    return info;
                }
                string[] roles = { };
                return new SessionInfo { Roles = roles };
            }
        }

        public bool IsPageOutter
        {
            get
            {
                return OUTTER_PAGES.Contains(AbsolutePath);
            }
        }

        public bool IsLoggedIn
        {
            get
            {
                return SessionService.GetSessionInfo(Session) != null;
            }

        }
    }
}