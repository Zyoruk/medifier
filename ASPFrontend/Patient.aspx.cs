﻿using System;
using System.Web.UI.WebControls;

namespace ASPFrontend
{
    public partial class Patient : System.Web.UI.Page
    {
        ApiGateway.ApiGatewayClient obj = new ApiGateway.ApiGatewayClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView.DataSource = obj.GetPatients();
            GridView.DataBind();
        }

        private void SavePatientDetails()
        {
            if (string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtLastName.Text)|| string.IsNullOrEmpty(txtDocument.Text) || string.IsNullOrEmpty(txtPhone.Text) || string.IsNullOrEmpty(txtEmail.Text))
            {
                lblError.Text = "Ingrese la información faltante, por favor!";
            }
            else
            {
                string name = txtName.Text;
                string lastname = txtLastName.Text;
                DateTime created = DateTime.Now;
                DateTime Modified = DateTime.Now;
                string Phone = txtPhone.Text;
                string Document = txtDocument.Text;
                string Email = txtEmail.Text;
                DateTime birthday = DateTime.Parse(txtBirth.Text);
                int Status = 3;
                string roles = "Paciente";

                obj.AddPatient(name, lastname, created, Modified, Phone, Document,Email, birthday, Status, roles);
                txtBirth.Text = "";
                txtDocument.Text = "";
                txtEmail.Text = "";
                txtLastName.Text = "";
                txtName.Text = "";
                txtPhone.Text = "";
                lblSuccess.Text = "Paciente agregado correctamente.";
            }
        }

        protected void Insert(object sender, EventArgs e)
        {
            SavePatientDetails();
        }

        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}