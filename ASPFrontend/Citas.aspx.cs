﻿using ASPFrontend.Services;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace ASPFrontend
{
    public partial class Citas : System.Web.UI.Page
    {
        private string[] authRoles = { "Sysadmin", "Administrativo", "Doctor", "Paciente" };

        public bool IsAuth
        {
            get
            {
                return SessionService.ContainsRole(Session, authRoles);
            }
        }


        ApiGateway.ApiGatewayClient datosCitas = new ApiGateway.ApiGatewayClient();

        public _BasePage page = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.page = new _BasePage(Request.Url.AbsolutePath, Session);
            if (!IsPostBack)
            {
                PopulateGridview();
            }
           
            //TextBox txtcitahora = gvcitas.FooterRow.FindControl("txtCitaHoraFooter") as TextBox;
            //txtcitahora.Enabled = false;
        }

        void PopulateGridview()
        {
            DataTable dtbl = new DataTable();
            ApiGateway.CitasInfo[] info = datosCitas.GetDataCitas(txtCedula.Text);
            if (info.Length > 0)
            {
                gvcitas.DataSource = info;
                gvcitas.DataBind();
            }
            else
            {
                dtbl.Columns.Add("IntIdCita");
                dtbl.Columns.Add("StrCedula");
                dtbl.Columns.Add("NombrePaciente");
                dtbl.Columns.Add("DateFechaCita");
                dtbl.Columns.Add("CitaHora");
                dtbl.Columns.Add("NameTratamiento");
                dtbl.Columns.Add("TrataPreciototal");
                dtbl.Columns.Add("Estado");
                dtbl.Rows.Add(dtbl.NewRow());
                gvcitas.DataSource = dtbl;
                gvcitas.DataBind();
                gvcitas.Rows[0].Cells.Clear();
                gvcitas.Rows[0].Cells.Add(new TableCell());
                gvcitas.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                gvcitas.Rows[0].Cells[0].Text = "Sin datos de citas ..!";
                gvcitas.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }

        }

        protected void gvcitas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvcitas.EditIndex = -1;
            PopulateGridview();
        }

        protected void gvcitas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddNew"))
                {

                    string cedula = (gvcitas.FooterRow.FindControl("txtStrCedulaFooter") as TextBox).Text.Trim();
                    int tratamiento = int.Parse((gvcitas.FooterRow.FindControl("DropDownList1") as DropDownList).SelectedValue.Trim());
                    DateTime fecha = DateTime.Parse((gvcitas.FooterRow.FindControl("txtDateFechaCitaFooter") as TextBox).Text.Trim());
                    decimal precio = 0;
                    string hora = (gvcitas.FooterRow.FindControl("DropHoras") as DropDownList).SelectedValue.Trim();
                    datosCitas.AgregarDataCitas(cedula, tratamiento, fecha, precio,hora);
                    PopulateGridview();
                }
            }
            catch (Exception)
            {
                
            }
        }

        protected void gvcitas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                datosCitas.EliminarCita(Convert.ToInt32(gvcitas.DataKeys[e.RowIndex].Value.ToString()));
                PopulateGridview();
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        protected void gvcitas_RowEditing(object sender, GridViewEditEventArgs e)
        { 
            gvcitas.EditIndex = e.NewEditIndex;
            PopulateGridview();
        }

        protected void gvcitas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int IdCita = Convert.ToInt32(gvcitas.DataKeys[e.RowIndex].Value.ToString());
            DateTime fecha = DateTime.Parse((gvcitas.Rows[e.RowIndex].FindControl("txtDateFechaCita") as TextBox).Text.Trim()); 
            string date = fecha.ToString("yyyy/MM/dd");
            string cedula = (gvcitas.Rows[e.RowIndex].FindControl("txtStrCedula") as TextBox).Text.Trim();
            string estado = (gvcitas.Rows[e.RowIndex].FindControl("DropEstado") as DropDownList).SelectedValue.Trim();
            int idtratamiento = int.Parse((gvcitas.Rows[e.RowIndex].FindControl("DropEditTratamiento") as DropDownList).SelectedValue.Trim());
            string nametratamiento = (gvcitas.Rows[e.RowIndex].FindControl("DropEditTratamiento") as DropDownList).SelectedItem.Text;
            if(idtratamiento == 0 && estado == "Atendido")
            {
                nametratamiento = (gvcitas.Rows[e.RowIndex].FindControl("txtIntIdTratamiento") as TextBox).Text.Trim();
            }
            string citahora = (gvcitas.Rows[e.RowIndex].FindControl("DropHoraEdit") as DropDownList).SelectedValue.Trim();

            datosCitas.ActualizarCita(IdCita,cedula,date,estado, idtratamiento, nametratamiento, citahora);
            PopulateGridview();
        }


        protected void gvcitas_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList DropDownList1 = (e.Row.FindControl("DropDownList1") as DropDownList);

                var tratamientos = datosCitas.GetTreatmentRecords();

                foreach (var item in tratamientos)
                {
                    DropDownList1.Items.Add(new ListItem(item.Name, item.ID.ToString()));
                }

                DropDownList1.Items.Insert(0, new ListItem("--Seleccion tratamiento--", "0"));

                DropDownList DropHoras = (e.Row.FindControl("DropHoras") as DropDownList);

                var horas = datosCitas.ContainsHoraSCita();

                foreach (var item in horas)
                {
                    DropHoras.Items.Add(new ListItem(item, item));
                }

                DropHoras.Items.Insert(0, new ListItem("--Seleccione una hora--", "0"));

            }
            else if (e.Row.RowState == DataControlRowState.Edit)
            {
              

                DropDownList DropEditTratamiento = (e.Row.FindControl("DropEditTratamiento") as DropDownList);

                var tratamientos = datosCitas.GetTreatmentRecords();

                foreach (var item in tratamientos)
                {
                    DropEditTratamiento.Items.Add(new ListItem(item.Name, item.ID.ToString()));
                }

                DropEditTratamiento.Items.Insert(0, new ListItem("--Seleccion tratamiento--", "0"));



                DropDownList DropHoraEdit = (e.Row.FindControl("DropHoraEdit") as DropDownList);

                var horas = datosCitas.ContainsHoraSCita();

                foreach (var item in horas)
                {
                    DropHoraEdit.Items.Add(new ListItem(item, item));
                }

                DropHoraEdit.Items.Insert(0, new ListItem("--Seleccion una hora--", "0"));
            }
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            PopulateGridview();
        }

        protected void txtCedula_TextChanged(object sender, EventArgs e)
        {

        }

        protected void DropHoras_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}