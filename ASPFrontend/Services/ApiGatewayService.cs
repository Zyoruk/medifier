﻿namespace ASPFrontend.Services
{
    public class ApiGatewayService
    {
        private static ApiGateway.IApiGateway apiGateway;
        public static ApiGateway.IApiGateway Instance
        {
            get
            {
                if (apiGateway == null)
                {
                    apiGateway = new ApiGateway.ApiGatewayClient();
                }
                return apiGateway;
            }
        }

    }
}