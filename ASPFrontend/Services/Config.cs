﻿using ASPFrontend.Wrappers;

namespace ASPFrontend.Services
{
    public class Config
    {
        public static string DefaultPage
        {
            get
            {
                return ConfigurationWrapper.ReadAppSetting("defaultPage");
            }
        }
    }
}