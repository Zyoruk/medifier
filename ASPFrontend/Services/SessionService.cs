﻿using ASPFrontend.ApiGateway;
using System;
using System.Web.SessionState;

namespace ASPFrontend.Services
{
    public class SessionService
    {
        public static SessionInfo GetSessionInfo(HttpSessionState session)
        {
            if (session["token"] == null) return null;
            return !string.IsNullOrEmpty(session["token"].ToString())
                ? ApiGatewayService.Instance.ValidateToken(session["token"].ToString())
                : null;
        }

        public static void SetSessionToken(string token, HttpSessionState session)
        {
            // Something wrong happened
            if (!string.IsNullOrEmpty(token))
            {
                session["token"] = token;
            }
            else
            {
                throw new Exception("Something happened on login");
            }
        }

        public static bool ContainsRole(HttpSessionState Session, params string[] roles)
        {
            var info = GetSessionInfo(Session);
            bool isAuth = false;
            foreach(var rol in roles)
            {
                foreach(var infoRole in info.Roles)
                {
                    if (infoRole == rol)
                    {
                        return true;
                    }
                }
            }
            return isAuth;
        }
    }
}