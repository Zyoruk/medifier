﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tratamiento.aspx.cs" Inherits="ASPFrontend.Tratamiento" MasterPageFile="_OutterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <title>Tratamientos</title>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <h1 style="font-family: Arial, Helvetica, sans-serif;">Tratamientos<br />
    </h1>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td colspan="2" style="margin:0; padding:0; border-bottom: none">
                    <asp:GridView CssClass="w-100 table" ID="grdWcfTest" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CellPadding="5"  OnRowDeleting="grdWcfTest_RowDeleting" OnRowEditing="grdWcfTest_RowEditing" OnRowUpdating="grdWcfTest_RowUpdating" OnSelectedIndexChanged="grdWcfTest_SelectedIndexChanged"  OnRowCancelingEdit="grdWcfTest_RowCancelingEdit">
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="ID" />
                            <asp:BoundField HeaderText="Nombre" DataField="Name" />
                            <asp:BoundField HeaderText="Precio" DataField="Price" DataFormatString="{0:0.00}" />
                            <asp:CommandField EditText="Editar" ShowEditButton="true" ButtonType="Link" />
                            <asp:CommandField DeleteText="Eliminar" ShowDeleteButton="true" ButtonType="Link" />
                        </Columns>
                    </asp:GridView>
                    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; margin: 0" class="w-100 table">
                        <tr>
                            <td>Nombre:<br />
                                <asp:TextBox CssClass="form-control" ID="txtName3" runat="server" Width="210" />
                            </td>
                            <td>Precio:<br />
                                <asp:TextBox CssClass="form-control" ID="txtPrice3" runat="server" Width="210" TextMode="Number" />
                            </td>
                            <td valign="bottom">
                                <asp:Button CssClass="btn btn-primary" ID="btnAdd" runat="server" Text="Agregar" OnClick="Insert" />
                            </td>
                        </tr>

                    </table>
                    <asp:Label ID="lblstatus" runat="server" ForeColor="Red" EnableViewState="False" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
