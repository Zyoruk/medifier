﻿using APIGateway.AppointmentsService;
using APIGateway.AuthService;
using APIGateway.Model;
using APIGateway.TreatmentsService;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace APIGateway
{
    [ServiceContract]
    public interface IApiGateway
    {
        [OperationContract]
        List<CitasInfo> GetDataCitas(string cedula);

        [OperationContract]
        void AgregarDataCitas(string cedula, int tratamiento, DateTime fecha, decimal precio,string cihora);

        [OperationContract]
        ApiModel GetDataApi(string IDdocumento);

        [OperationContract]
        void EliminarCita(int id);

        [OperationContract]
        void ActualizarCita(int id, string cedula, string fecha, string estado, int idtratamiento, string nametratam, string citahora);

        [OperationContract]
        List<TratamientosInfo> Obtenertratamiento();

        [OperationContract]
        string Login(string username, string password);

        [OperationContract]
        SessionInfo ValidateToken(string token);

        [OperationContract]
        void UpdateRoles(int id, params string[] newroles);

        [OperationContract]
        void AddTreatmentRecord(string name, decimal price);

        [OperationContract]
        List<MedifierRol> GetApiRol();

        [OperationContract]
        List<TreatmentService> GetTreatmentRecords();

        [OperationContract]
        void UpdateTreatmentRecord(int id, string name, decimal price);

        [OperationContract]
        UserDataModel UserDataModel (string iddocument);

        [OperationContract]
        void DeleteTreatment(int id);

        [OperationContract]
        List<string> ContainsHoraSCita();

        [OperationContract]
        void SaveDataFact(int idrecibo, string iduser, string formapago);

        [OperationContract]
        List<User> GetPatients();

        [OperationContract]
        void AddPatient(string firstname, string lastname, DateTime created, DateTime modified, string phone, string documentid, string email, DateTime birthdate, int status, string roles);
    }

    [DataContract]
    public class ApiRol
    {
        [DataMember]
        public string Rol { get; set; } 
    }

    [DataContract]
    public class ApiModel
    {
        [DataMember]
        public int Id { get; set; } = 0;

        [DataMember]
        public string FirstName { get; set; } = "";

        [DataMember]
        public string LastName { get; set; } = "";

        [DataMember]
        public string DocumentId { get; set; } = "";

        [DataMember]
        public string Phone { get; set; } = "";

        [DataMember]
        public DateTime Created { get; set; } = new DateTime();

        [DataMember]
        public DateTime Modified { get; set; } = new DateTime();

        [DataMember]
        public string Roles { get; set; } = "";

        [DataMember]
        public string Email { get; set; } = "";

        [DataMember]
        public int Status { get; set; } = 0;
    }
}
