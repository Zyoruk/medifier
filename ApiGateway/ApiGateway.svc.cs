﻿using APIGateway.AppointmentsService;
using APIGateway.AccountingService;
using APIGateway.AuthService;
using System;
using System.Collections.Generic;
using APIGateway.TreatmentsService;
using APIGateway.Model;

namespace APIGateway
{
    public class ApiGateway : IApiGateway
    {
        IAccountingService accService = new AccountingServiceClient();
        IAppointmentsService appService = new AppointmentsServiceClient();
        IAuthService authService = new AuthServiceClient();
        ITreatmentsService treatments = new TreatmentsServiceClient();

        public static decimal price;
        public static string name;

        public List<CitasInfo> GetDataCitas(string cedula)
        {
            var person = authService.Data(cedula);
            List<CitasInfo> datosCitas = new List<CitasInfo>(appService.ObtenerCitas(cedula));
            return datosCitas;
        }

        public void AgregarDataCitas(string cedula, int tratamiento, DateTime fecha,
            decimal precio, string cihora)
        {
            var Datostrata = treatments.GetPriceNameTreatmens(tratamiento);
            var person = authService.Data(cedula);

            foreach (var item in Datostrata)
            {
                price = item.Price;
                name = item.Name;
            }

            appService.AgregarCita(cedula, tratamiento, name, fecha, price, cihora,
                person.Firstname + person.Lastname);
        }

        public void EliminarCita(int id)
        {
            appService.EliminarCita(id);
        }

        public void ActualizarCita(int id, string cedula, string fecha,string estado, int idtratamiento,
            string nametratam, string citahora)
        {
            var Datostrata = treatments.GetPriceNameTreatmens(idtratamiento);

            foreach (var item in Datostrata)
            {
                price = item.Price;
            }

            appService.ActualizarCita(id, cedula, fecha, estado, idtratamiento, nametratam,
                price, citahora);

            if(estado == "Atendido")
            {
                accService.AgregarFactura(cedula, nametratam, price);
            }
        }

        public string Login(string username, string password)
        {
            if (String.IsNullOrEmpty(username) || (String.IsNullOrEmpty(password)))
            {
                //"Please be sure that the username and password are set"
                return null;
            }
            return authService.Authenticate(username, password);
        }

        public void UpdateRoles(int id, params string[] newroles)
        {
            authService.UpdateRol(id, newroles);
        }

        public SessionInfo ValidateToken (string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return null;
            }
            return authService.ValidateToken(token);
        }

        public List<TratamientosInfo> Obtenertratamiento()
        {
            List<TratamientosInfo> datostratamientos = new List<TratamientosInfo>(treatments.Obtenertratamientos());
            return datostratamientos;
        }

        public ApiModel GetDataApi(string IDdocumento)
        {
            ApiModel dt = null;

            Person d = authService.Data(IDdocumento);

            if (d != null)
            {
                dt = new ApiModel
                {
                    Id = Convert.ToInt32(d.Id),
                    FirstName = Convert.ToString(d.Firstname),
                    LastName = Convert.ToString(d.Lastname),
                    Roles = Convert.ToString(d.Roles),
                    Email = Convert.ToString(d.Email),
                    Phone = Convert.ToString(d.Phone) 

                };
            }
            else
            {

                dt = new ApiModel
                {
                    Id = 0,
                    FirstName = " ",
                    LastName = " ",
                    Roles = " ",
                    Email = "",
                    Phone = ""
                };
            }

            return dt;
        }

        public List<string> ContainsHoraSCita()
        {
            List<string> result = new List<string>(appService.ContainsHoraSCita());

            return result;
        }

        public List<MedifierRol> GetApiRol()
        {
            List<MedifierRol> consultaroles = new List<MedifierRol>(authService.ListRol());
            return consultaroles;
        }

        public UserDataModel UserDataModel(string iddocument)
        {
            var users = authService.Data(iddocument);
            var cobro = accService.GetFactData(iddocument);

            List<AccountingCobroData> rcobro = new List<AccountingCobroData>();

            foreach (var AccountingInfo in cobro)
            {
                rcobro.Add(new AccountingCobroData
                {
                    Idrecibo = AccountingInfo.Idrecibo,
                    Nametrata = AccountingInfo.Nametrata,
                    Costo = AccountingInfo.Costo,
                    Date = AccountingInfo.Date    
                });
            }

            UserDataModel response = new UserDataModel
            {
                User = users.Id,
                Firtsname = users.Firstname,
                Lastname = users.Lastname,
                Phone = users.Phone,
                Email = users.Email,
                Cobros = rcobro.ToArray()
            };

            return response;
        }

        public void SaveDataFact(int idrecibo, string iduser, string formapago)
        {
            accService.SaveDataFact(idrecibo, iduser, formapago);
            
        }


        public void AddTreatmentRecord(string name, decimal price)
        {
            treatments.AddTreatmentRecord(name, price);
        }

        public List<TreatmentService> GetTreatmentRecords()
        {
            List<TreatmentService> treatmentlist = new List<TreatmentService>(treatments.GetTreatmentRecords());
            return treatmentlist;
        }

        public void DeleteTreatment(int id)
        {
            treatments.DeleteRecords(id);
        }

        public void UpdateTreatmentRecord(int id, string name, decimal price)
        {
            treatments.UpdateTreatmentContact(id, name, price);
        }

        public void AddPatient(string firstname, string lastname, DateTime created, DateTime modified, string phone, string documentid, string email,  DateTime birthdate, int status, string roles)
        {
            authService.AddPatient(firstname, lastname, created, modified, phone, documentid, email, birthdate, status, roles);
        }

        public List<User> GetPatients()
        {
            List<User> patientlist = new List<User>(authService.GetPatients());
            return patientlist;
        }
    }
}
