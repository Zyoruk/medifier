﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIGateway.Model
{
    public class AccountingModel
    {

        public int idcita { get; set; }
        public int idtrata { get; set; }
        public string date { get; set; }
        public string iddocument { get; set; }

        public string tratamient { get; set; }
        public string proce { get; set; }

        public string firsname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

    }
}