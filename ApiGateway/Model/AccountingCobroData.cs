﻿using System;

namespace APIGateway.Model
{
    public class UserDataModel
    {
        public int User { get; set; }
        public string Iddocument { get; set; }
        public string Firtsname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public AccountingCobroData[] Cobros { get; set; }
    }

    public class AccountingCobroData {

       // public string Iddocument { get; set; }
        public int Idrecibo { get; set; }
        public string Nametrata { get; set;}
        public decimal Costo { get; set; }
        public string Estado { get; set; }
        public DateTime Date { get; set; }   
    }

}