﻿using System;
using System.Data.Linq.Mapping;

namespace AppointmentsService
{
    [Table(Name = "Citas")]
    public class DatosCitas
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, Name = "IntIdCita")]
        public int IntIdCita { get; set; }

        [Column(Name = "StrCedula")]
        public string StrCedula { get; set; }

        [Column(Name = "IntIdTratamiento")]
        public int IntIdTratamiento { get; set; }

        [Column(Name = "DateFechaCita")]
        public DateTime DateFechaCita { get; set; }

        [Column(Name = "TrataPreciototal")]
        public decimal TrataPreciototal { get; set; }

        [Column(Name = "CitaHora")]
        public string CitaHora { get; set; }

        [Column(Name = "Estado")]
        public string Estado { get; set; }

        [Column(Name = "NameTratamiento")]
        public string NameTratamiento { get; set; }

        [Column]
        public string NombrePaciente { get; set; }
    }
}