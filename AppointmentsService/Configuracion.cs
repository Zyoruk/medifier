﻿using System.Configuration;

namespace AppointmentsService
{
    public class Configuracion
    {
        public static string CadenaConexion
        {
            get
            {
                return ConfigurationManager
                    .ConnectionStrings["CadenaConexion"]
                    .ConnectionString
                    .ToString();
            }
        }
    }
}