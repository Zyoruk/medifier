﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;


namespace AppointmentsService
{
    
    [ServiceContract]
    public interface IAppointmentsService
    {

        [OperationContract]
        List<CitasInfo> ObtenerCitas(string cedula);

        [OperationContract]
        void AgregarCita(string cedula, int tratamiento, string nombretratamiento, DateTime fecha, decimal precio, string CitaHora, string NombrePaciente);

        [OperationContract]
        void ActualizarCita(int id, string cedula, string fecha,string estado, int idtratamiento, string nametratam, decimal precio, string citahora);

        [OperationContract]
        void EliminarCita(int id);

        [OperationContract]
        List<string> ContainsHoraSCita();
    }

    [DataContract]
    public class CitasInfo
    {
        [DataMember]
        public int IntIdCita { get; set; }

        [DataMember]
        public string StrCedula { get; set; }

        [DataMember]
        public string NombrePaciente { get; set; }

        [DataMember]
        public string DateFechaCita { get; set; }

        [DataMember]
        public decimal TrataPreciototal { get; set; }

        [DataMember]
        public string CitaHora { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public int IdTratamiento { get; set; }

        [DataMember]
        public string NameTratamiento { get; set; }
    }  
}
