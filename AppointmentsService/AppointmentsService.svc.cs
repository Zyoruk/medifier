﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppointmentsService
{
    public class AppointmentsService : IAppointmentsService
    {
   
        Citas bd = new Citas();

        public void AgregarCita(string cedula, int tratamiento,string nombretratamiento,DateTime fecha, decimal precio, string CiHora, string nombrePaciente)
        {
            DatosCitas nuevaCita
                = new DatosCitas { StrCedula = cedula, IntIdTratamiento = tratamiento, DateFechaCita = fecha, TrataPreciototal=precio, CitaHora=CiHora,Estado ="Pendiente",NameTratamiento = nombretratamiento , NombrePaciente = nombrePaciente};
            bd.Cita.InsertOnSubmit(nuevaCita);
            bd.SubmitChanges();
        }

        public List<CitasInfo> ObtenerCitas(string cedula)
        {
            List<CitasInfo> dcitas = new List<CitasInfo>();

            if (cedula == "")
            {

                var consulta = from cl in bd.Cita
                               where cl.Estado != "Atendido"
                               select new { cl.NombrePaciente, cl.IntIdCita,cl.StrCedula, cl.DateFechaCita, cl.NameTratamiento, cl.TrataPreciototal, cl.CitaHora, cl.Estado };

                foreach (var cita in consulta)
                {
                    dcitas.Add(new CitasInfo
                    {
                        IntIdCita = cita.IntIdCita,
                        NameTratamiento = cita.NameTratamiento,
                        StrCedula = cita.StrCedula,
                        DateFechaCita = cita.DateFechaCita.ToString("dd/MM/yyyy"),
                        TrataPreciototal = cita.TrataPreciototal,
                        CitaHora = cita.CitaHora,
                        Estado = cita.Estado,
                        NombrePaciente = cita.NombrePaciente
                    });
                }
            }
            else
            {
                var consulta = from cl in bd.Cita
                               where cl.StrCedula.Equals(cedula) 
                               where cl.Estado != "Atendido"
                               select new { cl.IntIdCita, cl.StrCedula, cl.DateFechaCita, cl.NameTratamiento, cl.TrataPreciototal, cl.CitaHora, cl.Estado };



                foreach (var cita in consulta)
                {
                    dcitas.Add(new CitasInfo
                    {
                        IntIdCita = cita.IntIdCita,
                        NameTratamiento = cita.NameTratamiento,
                        StrCedula = cita.StrCedula,
                        DateFechaCita = cita.DateFechaCita.ToString("dd/MM/yyyy"),
                        TrataPreciototal = cita.TrataPreciototal,
                        CitaHora = cita.CitaHora,
                        Estado = cita.Estado
                    });
                }
            }

            return dcitas;
        }

        public void EliminarCita(int id)
        {
            DatosCitas cita = bd.Cita.Single(c => c.IntIdCita == id);
            bd.Cita.DeleteOnSubmit(cita);
            bd.SubmitChanges();
        }

        public void ActualizarCita(int id, string cedula, string fecha,string estado,int idtratamiento,string nametratam,decimal precio,string citahora)
        {

        
            if(citahora == "0" && idtratamiento != 0)
            {
                DatosCitas cita = bd.Cita.Single(c => c.IntIdCita == id);
                cita.StrCedula = cedula;
                DateTime dateTime = DateTime.Parse(fecha);
                cita.DateFechaCita = dateTime;
                cita.Estado = estado;
                cita.IntIdTratamiento = idtratamiento;
                cita.NameTratamiento = nametratam;
                cita.TrataPreciototal = precio;
                bd.SubmitChanges();
            }
            else if (citahora == "0" && idtratamiento == 0)
            {
                DatosCitas cita = bd.Cita.Single(c => c.IntIdCita == id);
                cita.StrCedula = cedula;
                DateTime dateTime = DateTime.Parse(fecha);
                cita.DateFechaCita = dateTime;
                cita.Estado = estado;
                bd.SubmitChanges();
            }
            else if (citahora != "0" && idtratamiento != 0)
            {
                DatosCitas cita = bd.Cita.Single(c => c.IntIdCita == id);
                cita.StrCedula = cedula;
                DateTime dateTime = DateTime.Parse(fecha);
                cita.DateFechaCita = dateTime;
                cita.Estado = estado;
                cita.IntIdTratamiento = idtratamiento;
                cita.NameTratamiento = nametratam;
                cita.TrataPreciototal = precio;
                cita.CitaHora = citahora;
                bd.SubmitChanges();
            }
         
            else if (citahora != "0" && idtratamiento == 0)
            {
                DatosCitas cita = bd.Cita.Single(c => c.IntIdCita == id);
                cita.StrCedula = cedula;
                DateTime dateTime = DateTime.Parse(fecha);
                cita.DateFechaCita = dateTime;
                cita.Estado = estado;
                cita.CitaHora = citahora;
                bd.SubmitChanges();
            }

        }


        public List<string> ContainsHoraSCita()
        {
            List<string> Horas = new List<string>()
            {
                "9 a.m",
                "10 a.m",
                "11 a.m",
                "12 p.m",
                "1 p.m",
                "2 p.m",
                "3 p.m",
                "4 p.m"
            };
            List<string> HorasOcupadas = new List<string>();
            List<string> result = new List<string>();


            var consulta = from cl in bd.Cita                      
                           select new {cl.CitaHora};


            foreach (var cita in consulta)
            {
                HorasOcupadas.Add(cita.CitaHora.ToString());
            }


            result.AddRange(HorasOcupadas.Except(Horas, StringComparer.OrdinalIgnoreCase));
            result.AddRange(Horas.Except(HorasOcupadas, StringComparer.OrdinalIgnoreCase));

            return result;
        }

    };

}

