﻿using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace AppointmentsService
{
    [Database(Name = "AppointmentsDB")]
    public class Citas : DataContext
    {
        public Citas() : base(Configuracion.CadenaConexion) { }
        public Table<DatosCitas> Cita;
    }
}