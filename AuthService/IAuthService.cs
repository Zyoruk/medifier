﻿using AuthService.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace AuthService
{
    [ServiceContract]
    public interface IAuthService
    {
        [OperationContract]
        string Authenticate(string uniqueIdentifier, string password);

        [OperationContract]
        Person Data(string DocumentId);

        [OperationContract]
        List<MedifierRol> ListRol();

        [OperationContract]
        void UpdateRol(int id, params string[] newroles);

        [OperationContract]
        SessionInfo ValidateToken(string token);

        [OperationContract]
        void AddPatient(string firstname, string lastname, DateTime created, DateTime modified, string phone, string documentid, string email, DateTime birthdate, int status, string roles);

        [OperationContract]
        List<User> GetPatients();
    }


    [DataContract]
    public class Credentials
    {
        [DataMember]
        public int UserId { get; set; } = 0;

        [DataMember]
        public string Password { get; set; } = "";
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; } = 0;

        [DataMember]
        public string FirstName { get; set; } = "";

        [DataMember]
        public string LastName { get; set; } = "";

        [DataMember]
        public string DocumentId { get; set; } = "";

        [DataMember]
        public string Phone { get; set; } = "";

        [DataMember]
        public DateTime Created { get; set; } = new DateTime();

        [DataMember]
        public DateTime Modified { get; set; } = new DateTime();

        [DataMember]
        public int Status { get; set; } = 0;

        [DataMember]
        public string Roles { get; set; } = "";

        [DataMember]
        public string  Email { get; set; } = "";
    }

    [DataContract]
    public class SessionInfo
    {
        [DataMember]
        public int UserId { get; set; } = 0;

        [DataMember]
        public string[] Roles { get; set; } = { };

        [DataMember]
        public string Username { get; set; } = "";

        [DataMember]
        public string DocumentId { get; set; } = "";
    }
}
