﻿using System.Configuration;

namespace AuthService
{
    public class Config
    {
        public static string CadenaConexion
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CadenaConexion"]
                        .ConnectionString.ToString();
            }
        }
    }
}