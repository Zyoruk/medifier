﻿using System.Data.Linq.Mapping;

namespace AuthService.Models
{
    [Table(Name = "User")]
    public class User
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column]
        public string Password { get; set; }
    }
}