﻿
using System.Data.Linq.Mapping;


namespace AuthService
{
    [Table(Name = "Roles")]
    public class MedifierRol
    {
        [Column]
        public string Rol { get; set; }
    }
}