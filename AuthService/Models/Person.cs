﻿using System;
using System.Data.Linq.Mapping;

namespace AuthService.Models
{
    [Table(Name = "Person")]
    public class Person
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id;

        [Column]
        public string Firstname;

        [Column]
        public string Lastname;

        [Column]
        public DateTime Birthdate;

        [Column]
        public DateTime Created;

        [Column]
        public DateTime Modified;

        [Column]
        public string Phone;

        [Column]
        public string DocumentId;

        [Column]
        public string Email;

        [Column]
        public int Status;

        [Column]
        public int UserId;

        [Column]
        public string Roles { get; set; } = "";
    }
}