﻿namespace AuthService.Models.Dto
{
    public class SessionInfo
    {
        public int UserId { get; set; }
        public string[] Roles { get; set; }
        public string Username { get; set; }
    }
}