﻿using System;

namespace AuthService.Models.Dto
{
    public class PersonDto
    {

        public int Id;

        public string Firstname;

        public string Lastname;

        public DateTime Birthdate;

        public DateTime Created;

        public DateTime Modified;
        
        public string Phone;

        public string DocumentId;

        public string Email;

        public int UserId;

        public int Status;

        public string Roles;
    }
}
