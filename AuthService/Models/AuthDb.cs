﻿using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace AuthService.Models
{
    [Database(Name = "AuthDB")]
    public class AuthDb: DataContext
    {
        private static AuthDb _instance;
        public static AuthDb Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthDb();
                }
                return _instance;
            }
        }

        public AuthDb(): base(Config.CadenaConexion) { }
        public Table<Person> Person;
        public Table<User> User;
        public Table<MedifierRol> MedifierRols;
    }
}