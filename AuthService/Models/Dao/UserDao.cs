﻿using AuthService.Models.Dto;
using System.Linq;

namespace AuthService.Models.Dao
{
    public class UserDao
    {
        public static UserDto GetUserBy(int userId)
        {
            User user = AuthDb.Instance.User.Single(p => p.Id == userId);
            return new UserDto
            {
                UserId = user.Id,
                Password = user.Password,
            };
        }
    }
}