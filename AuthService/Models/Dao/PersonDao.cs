﻿using AuthService.Models.Dto;
using System.Linq;

namespace AuthService.Models.Dao
{
    public class PersonDao
    {
        public static PersonDto GetPersonBy(string documentId)
        {
            try
            {
                Person person = AuthDb.Instance.Person.Single(p => p.DocumentId == documentId);
                return new PersonDto
                {
                    Id = person.Id,
                    DocumentId = person.DocumentId,
                    Birthdate = person.Birthdate,
                    Created = person.Created,
                    Email = person.Email,
                    Firstname = person.Firstname,
                    Lastname = person.Lastname,
                    Modified = person.Modified,
                    Phone = person.Phone,
                    UserId = person.UserId,
                    Status = person.Status,
                    Roles = person.Roles
                };
            } catch
            {
                return null;
            }
        }
    }
}