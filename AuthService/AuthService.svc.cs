﻿using AuthService.Models.Dao;
using AuthService.Models.Dto;
using AuthService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuthService
{
    
    public class AuthService : IAuthService
    {
        public Person Data(string idDoc)
        {
            Person dt = null;

            var consulta = from cl in AuthDb.Instance.Person
                           where cl.DocumentId == idDoc
                           select cl;


            foreach (var q in consulta)
            {
                dt = new Person
                {

                    Id = Convert.ToInt32(q.Id),
                    Firstname = Convert.ToString(q.Firstname),
                    Lastname = Convert.ToString(q.Lastname),
                    DocumentId = Convert.ToString(q.DocumentId),
                    Phone = Convert.ToString(q.Phone),
                    Created = Convert.ToDateTime(q.Created),
                    Modified = Convert.ToDateTime(q.Modified),
                    Roles = Convert.ToString(q.Roles),
                    Status = Convert.ToInt32(q.Status),
                    Email = Convert.ToString(q.Email)
                };
            }

            return dt;
        }

        private bool ValidatePassword(Credentials creds)
        {
            UserDto user = UserDao.GetUserBy(creds.UserId);
            return user.Password == creds.Password;
        }

        public List<MedifierRol> ListRol()
        {
            var consulta = from cl in AuthDb.Instance.MedifierRols
                           select cl;


            return consulta.ToList();
        }


        public void UpdateRol(int id, params string[] newroles)
        {
            Person data = AuthDb.Instance.Person.Single(c => c.Id == id);
            data.Roles = string.Join(",", newroles);
            data.Modified = DateTime.Now;
            AuthDb.Instance.SubmitChanges();
        }

        public string Authenticate(string uniqueIdentifier, string password) {
            if (String.IsNullOrEmpty(uniqueIdentifier) || String.IsNullOrEmpty(password))
            {
                throw new Exception("Params cannot be null");
            }

            PersonDto person = PersonDao.GetPersonBy(uniqueIdentifier);
            if (person != null && ValidatePassword(new Credentials { Password = password, UserId = person.UserId })) {
                Dictionary<string, object> userClaims = new Dictionary<string, object>
                {
                    { "created", person.Created },
                    { "modified" , person.Modified },
                    { "status", person.Status },
                    { "firstname", person.Firstname },
                    { "lastname", person.Lastname },
                    { "phone", person.Phone },
                    { "email", person.Email },
                    { "userid", person.UserId },
                    { "documentid", person.DocumentId },
                    { "roles", person.Roles.Split(',') }
                };
                string jwt_token = JWTWrapper.GenerateJWT(person.Firstname, person.UserId, userClaims);
                return jwt_token;
            }
            return null;
        }

        public SessionInfo ValidateToken (string token)
        {
            SessionInfo info = JWTWrapper.ValidateToken(token);
            if (info == null) return null;
            return info;
        }

        public Models.User AddUser(string password)
        {
            Models.User patient = new Models.User { Password = password};

            AuthDb.Instance.User.InsertOnSubmit(patient);
            AuthDb.Instance.SubmitChanges();
            return AuthDb.Instance.User.OrderByDescending(u => u.Id).FirstOrDefault();

        }

        public void AddPatient(string firstname, string lastname, DateTime created, DateTime modified, string phone, string documentid, string email, DateTime birthdate, int status, string roles)
        {
            Models.User user = AddUser("5f4dcc3b5aa765d61d8327deb882cf99");
            Models.Person patient = new Models.Person {Firstname = firstname, Lastname = lastname, Created = created, Modified = modified, Phone = phone , DocumentId = documentid, Email = email, UserId = user.Id, Birthdate = birthdate, Status = status, Roles = roles };

            AuthDb.Instance.Person.InsertOnSubmit(patient);
            AuthDb.Instance.SubmitChanges();
        }

        public List<User> GetPatients()
        {
            var consulta = from cl in AuthDb.Instance.Person
                           where cl.Roles.Contains("Paciente")
                           select new
                           {
                                firstname = cl.Firstname,
                                lastname =  cl.Lastname,
                                documentid = cl.DocumentId,
                                phone = cl.Phone
                           };
            List<User> user = new List<User>();
            foreach (var lista in consulta)
            {
                user.Add(new User { FirstName = lista.firstname,
                    LastName = lista.lastname,
                    DocumentId = lista.documentid,
                    Phone = lista.phone});
            }
                   
            return user;
        }

    }
}
