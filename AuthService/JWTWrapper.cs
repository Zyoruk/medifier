﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Linq;

namespace AuthService
{
	public class JWTWrapper
    {
       private const string KEY = "5A2E109F7FEC74C136601C1B5F4235BCCCB790552681B9B905218F7D20D32DA2";

        public static string GenerateJWT(string username, int userId, Dictionary<string, object> claimsDictionary)
        {
            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Name, username),
                    new Claim(ClaimTypes.PrimarySid, userId.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY)),
                    SecurityAlgorithms.HmacSha256Signature),
                Claims = claimsDictionary
            };
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
            string jwt_token = handler.WriteToken(token);
            return jwt_token;
        }

        public static ClaimsPrincipal GetPrincipal(string token) {
            try {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                        RequireExpirationTime = true,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY))
                };

                ClaimsPrincipal principal = tokenHandler.ValidateToken(token,
                    parameters, out SecurityToken securityToken);
                return principal;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Dictionary<string, object> GetClaims(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY))
                };

                ClaimsPrincipal principal = tokenHandler.ValidateToken(token,
                    parameters, out SecurityToken securityToken);
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static SessionInfo ValidateToken(string token)
        {
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            Claim usernameClaim = identity.FindFirst(ClaimTypes.Name);
            List<Claim> rolesClaim =  identity.FindAll(c => c.Type == ClaimTypes.Role).ToList();
            var w1 = identity.Claims.Select(c => c.Type == "roles");
            List<string> roles = new List<string>();
            foreach (var role in rolesClaim) roles.Add(role.Value);
            Claim userIdClaim = principal.FindFirst("userId");
            Claim documentIdClaim = principal.FindFirst("documentid");
            return new SessionInfo
            {
                Roles = roles.ToArray(),
                Username = usernameClaim.Value,
                UserId = Convert.ToInt32(userIdClaim.Value),
                DocumentId = documentIdClaim.Value
            };
        }
    }
}