

create database Medifier

go

use Medifier

go

create table Paciente 
(
IntIdPaciente int  identity(1,1) not null,
StrCedula varchar(20) primary key  not null,
StrNombre varchar(50) 
)
go
create table Tratamientos 
(
IDTratamiento int primary key identity(1,1) not null,
Name varchar(50),
Price money 
)
go
create table Citas
(
IntIdCita int primary key identity(1,1),
IntIdTratamiento int FOREIGN KEY REFERENCES Tratamientos(IDTratamiento),
DateFechaCita datetime,
TrataPreciototal decimal,
StrCedula varchar(20) FOREIGN KEY REFERENCES Paciente(StrCedula)
)
go
